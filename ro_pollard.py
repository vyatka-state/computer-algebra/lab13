from random import Random
from math import gcd


RANDOM = Random()


def f(x, n: int):
    return int(x * x + 1) % n


def q_pollard_factorize(n: int, iterations_count=None):
    if iterations_count is None:
        iterations_count = n

    X = [RANDOM.randint(2, 10)]
    for i in range(1, iterations_count + 2):
        X.append(f(X[i - 1], n))

    for i in range(iterations_count):
        for k in range(1, i + 2):
            for j in range(i + 1):
                curr_gcd = gcd(abs(X[j] - X[k]), n)
                if 1 < curr_gcd < n:
                    return curr_gcd, n // curr_gcd

    return 1, n


def main():
    n = int(input('Введите число: '))
    if n < 9:
        print('Число должно быть больше 8')
        return
    if n % 2 == 0:
        print(f"{n} = {n // 2} * 2")
        return

    x, y = q_pollard_factorize(n)
    print(f'{n} = {x} * {y}')


if __name__ == '__main__':
    main()
