from math import sqrt


def fermat_factorize(n: int):
    x = int(sqrt(n))
    y = 0
    r = x * x - y * y - n

    while r != 0:
        if r > 0:
            y += 1
        else:
            x += 1
        r = x * x - y * y - n

    return x - y, x + y


def main():
    n = int(input('Введите число: '))
    if n < 2:
        print('Число должно быть больше 1')
        return
    if n % 2 == 0:
        print(f"{n} = {n // 2} * 2")
        return

    x, y = fermat_factorize(n)
    print(f'{n} = {x} * {y}')


if __name__ == '__main__':
    main()
