from math import sqrt, isqrt, gcd


def sherman_leman_factorize(n: int):
    for a in range(2, int(pow(n, 1 / 3))):
        if n % a == 0:
            return a, n // a

    for k in range(1, int(pow(n, 1 / 3))):
        for d in range(int(pow(n, 1 / 6) / (4 * sqrt(k))) + 2):
            sqr = pow(int(sqrt(4 * k * n)) + d, 2) - 4 * k * n
            if sqr >= 1 and sqr == isqrt(sqr) ** 2:
                A = int(sqrt(4 * k * n)) + d
                B = isqrt(A * A - 4 * k * n)
                A_plus_B_GCD = gcd(A + B, n)
                A_minus_B_GCD = gcd(A - B, n)
                if A_plus_B_GCD != 1 and A_minus_B_GCD != 1 and A_plus_B_GCD != n and A_minus_B_GCD != n:
                    return A_minus_B_GCD, n // A_minus_B_GCD

    return 1, n


def main():
    n = int(input('Введите число: '))
    if n < 9:
        print('Число должно быть больше 8')
        return
    if n % 2 == 0:
        print(f"{n} = {n // 2} * 2")
        return

    x, y = sherman_leman_factorize(n)
    print(f'{n} = {x} * {y}')


if __name__ == '__main__':
    main()
